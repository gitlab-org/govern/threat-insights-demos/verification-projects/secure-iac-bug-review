#!/bin/bash

NAMESPACE=notes-app

kubectl get namespace $NAMESPACE
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "Error: Could not find namespace '$NAMESPACE', will try to create"
    kubectl create namespace $NAMESPACE
    retVal=$?
    if [ $retVal -ne 0 ]; then
        echo "Error: Could not create namespace '$NAMESPACE'"
        exit $retVal
    fi
fi