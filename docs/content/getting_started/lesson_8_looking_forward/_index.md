---
bookCollapseSection: false
weight: 100
---

# Looking Forward

Thanks for completing this tutorial! I really hope you enjoyed it. If you are interested in purchasing GitLab and working with our sales team, you can [chat with us!]()

## GitLab Security Direction and New Features

You can see the direction GitLab is taking by checking out the [Secure Direction]() as well as the [Govern Direction]().

## Connecting on Social

## See us at a Conference

## Learn more about our Security Products

- Compliance Management


{{< button relref="/" >}}Go Home{{< /button >}}